import * as cv from 'opencv4nodejs';

export interface ITracked {
		id: number;
		journey: cv.Rect[];
}
