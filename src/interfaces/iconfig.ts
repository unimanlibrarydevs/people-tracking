import { CommonsType } from 'tscommons-core';

import { IConfig as IBlobDetectorConfig, isIConfig as isIBlobDetectorConfig } from '../classes/blob-detector.class';
import { IConfig as IBlobTrackerConfig, isIConfig as isIBlobTrackerConfig } from '../classes/blob-tracker.class';
import { IConfig as IVideoSourceConfig, isIConfig as isIVideoSourceConfig } from '../classes/video-source.class';
import { IConfig as IWebcamSourceConfig, isIConfig as isIWebcamSourceConfig } from '../classes/webcam-source.class';

export interface IConfig {
	source: IVideoSourceConfig|IWebcamSourceConfig,
	detector: IBlobDetectorConfig,
	tracker: IBlobTrackerConfig
}
export function isIConfig(test: unknown): test is IConfig {
	if (
			!CommonsType.hasPropertyT<IVideoSourceConfig>(test, 'source', isIVideoSourceConfig)
			&& !CommonsType.hasPropertyT<IWebcamSourceConfig>(test, 'source', isIWebcamSourceConfig)
	) return false;
	
	if (!CommonsType.hasPropertyT<IBlobDetectorConfig>(test, 'detector', isIBlobDetectorConfig)) return false;
	if (!CommonsType.hasPropertyT<IBlobTrackerConfig>(test, 'tracker', isIBlobTrackerConfig)) return false;
	
	return true;
}
