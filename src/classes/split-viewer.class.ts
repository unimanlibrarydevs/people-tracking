import * as cv from 'opencv4nodejs';

const DEFAULT_WINDOW_WIDTH: number = 320;
const DEFAULT_WINDOW_HEIGHT: number = 240;

export class SplitViewer {
	private readonly subWindowsX: number;
	private readonly subWindowsY: number;
	private readonly internalFrame: cv.Mat;
	
	constructor(
			private matrix: string[][],
			private readonly subWindowsWidth: number = DEFAULT_WINDOW_WIDTH,
			private readonly subWindowsHeight: number = DEFAULT_WINDOW_HEIGHT
	) {
		if (matrix.length === 0 || matrix[0].length === 0) throw new Error('Invalid matrix');
		this.subWindowsY = matrix.length;
		this.subWindowsX = matrix[0].length;
		
		this.internalFrame = new cv.Mat(
				this.subWindowsY * this.subWindowsHeight,
				this.subWindowsX * this.subWindowsWidth,
				cv.CV_8UC3
		);
	}
	
	private getPosition(window: string): cv.Point2|undefined {
		for (let y = 0; y < this.matrix.length; y++) {
			for (let x = 0; x < this.matrix[y].length; x++) {
				if (window === this.matrix[y][x]) return new cv.Point2(x, y);
			}
		}
		
		return undefined;
	}
	
	public drawWindow(
			window: string,
			frame: cv.Mat,
			format?: number
	): void {
		const position: cv.Point2|undefined = this.getPosition(window);
		if (position === undefined) return;
		
		if (format === cv.COLOR_GRAY2BGR) frame = frame.cvtColor(cv.COLOR_GRAY2BGR);
		
		const resized: cv.Mat = frame.resize(
				this.subWindowsHeight,
				this.subWindowsWidth
		);

		const rect: cv.Rect = new cv.Rect(
				position.x * this.subWindowsWidth,
				position.y * this.subWindowsHeight,
				this.subWindowsWidth,
				this.subWindowsHeight
		);

		const dest: cv.Mat = this.internalFrame.getRegion(rect);

		resized.copyTo(dest);

		dest.putText(
				window,
				new cv.Point2(5, 21),
				cv.FONT_HERSHEY_SIMPLEX,
				0.8,
				new cv.Vec3(255, 0, 255),
				2
		);
	}
	
	public display(duration: number): number|undefined {
		cv.imshow('frame', this.internalFrame);
		
		const key: number = cv.waitKey(duration);
		if (key !== -1 && key !== 255) return key;
		
		return undefined;
	}
}
