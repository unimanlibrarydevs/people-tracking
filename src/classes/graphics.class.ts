import * as cv from 'opencv4nodejs';

import { EDirection } from '../enums/edirection';

export class Graphics {
	private copy: cv.Mat;
	
	constructor(
			source: cv.Mat
	) {
		this.copy = source.copy();
	}
	
	public getFrame(): cv.Mat {
		return this.copy;
	}
	
	public drawDetections(detections: cv.Rect[]): void {
		for (const detection of detections) {
			this.copy.drawRectangle(
					detection,
					new cv.Vec3(255, 0, 0),
					2
			);
		}
	}
	
	public drawJourney(steps: cv.Point2[]): void {
		if (steps.length < 2) return;
		
		const clone: cv.Point2[] = steps.slice();

		let last: cv.Point2 = clone.shift()!;
		for (const step of clone) {
			this.copy.drawLine(
					last, step,
					new cv.Vec3(0, 0, 255),
					2
			);
			last = step;
		}
	}
	
	public drawJourneys(journeys: cv.Point2[][]): void {
		for (const steps of journeys) this.drawJourney(steps);
	}
	
	public drawThreshold(y: number): void {
		this.copy.drawLine(
				new cv.Point2(0, y),
				new cv.Point2(1000, y),
				new cv.Vec3(0, 255, 255),
				2
		);
	}
	
	public drawTracked(first: cv.Point2, last: cv.Point2, state: EDirection|undefined, i?: number): void {
		let color: cv.Vec3 = new cv.Vec3(255, 0, 0);
		if (state !== undefined) {
			switch (state) {
				case EDirection.UP:
					// in
					color = new cv.Vec3(0, 255, 0);
					break;
				case EDirection.DOWN:
					// in
					color = new cv.Vec3(0, 0, 255);
					break;
			}
		}
		
		this.copy.drawLine(first, last, color, 1);
		if (i === 0) this.copy.drawCircle(first, 5, color, -1);
		this.copy.drawCircle(last, 5, color, -1);
	}
	
	public drawTallies(tallyIn: number, tallyOut: number, tallyNet: number): void {
		this.copy.putText(
				'IN',
				new cv.Point2(10, 80),
				cv.FONT_HERSHEY_SIMPLEX,
				1,
				new cv.Vec3(0, 255, 0),
				2
		);
		this.copy.putText(
				tallyIn.toString(),
				new cv.Point2(100, 80),
				cv.FONT_HERSHEY_SIMPLEX,
				1,
				new cv.Vec3(0, 255, 0),
				2
		);
	
		this.copy.putText(
				'OUT',
				new cv.Point2(10, 110),
				cv.FONT_HERSHEY_SIMPLEX,
				1,
				new cv.Vec3(0, 0, 255),
				2
		);
		this.copy.putText(
				tallyOut.toString(),
				new cv.Point2(100, 110),
				cv.FONT_HERSHEY_SIMPLEX,
				1,
				new cv.Vec3(0, 0, 255),
				2
		);
	
		this.copy.putText(
				'NET',
				new cv.Point2(10, 140),
				cv.FONT_HERSHEY_SIMPLEX,
				1,
				new cv.Vec3(0, 255, 255),
				2
		);
		this.copy.putText(
				tallyNet.toString(),
				new cv.Point2(100, 140),
				cv.FONT_HERSHEY_SIMPLEX,
				1,
				new cv.Vec3(0, 255, 255),
				2
		);
	}
}
