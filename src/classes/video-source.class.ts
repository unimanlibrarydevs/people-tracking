import * as cv from 'opencv4nodejs';

import { CommonsType } from 'tscommons-core';

import { CommonsFile } from 'nodecommons-file';

import { Source } from './source.class';

export interface IConfig {
		filename: string
}
export function isIConfig(test: unknown): test is IConfig {
	if (!CommonsType.hasPropertyString(test, 'filename')) return false;
	
	return true;
}

export class VideoSource extends Source {
	public static fromConfig(
			config: IConfig
	): VideoSource {
		return new VideoSource(
				config.filename
		);
	}
	
	private capture: cv.VideoCapture;
	
	constructor(
			filename: string
	) {
		super();
		
		if (!CommonsFile.exists(filename)) throw new Error('No such video source file exists');

		this.capture = new cv.VideoCapture(filename);
	}
	
	public nextFrame(): cv.Mat|undefined {
		const frame: cv.Mat = this.capture.read();
		if (frame.empty) return undefined;
		
		return frame;
	}
	
	public getFrameRate(): number {
		return 30;
	}
}
