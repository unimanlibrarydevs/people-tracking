import * as cv from 'opencv4nodejs';

import { CommonsType } from 'tscommons-core';

import { MixedBackgroundSubtractor } from './mixed-background-subtractor.class';
import { SplitViewer } from './split-viewer.class';

const DEFAULT_MOG2_HISTORY: number = 500;
const DEFAULT_MOG2_THRESHOLD: number = 16;
const DEFAULT_MOG2_DETECT_SHADOWS: boolean = true;
const DEFAULT_KNN_HISTORY: number = 500;
const DEFAULT_KNN_THRESHOLD: number = 400;
const DEFAULT_KNN_DETECT_SHADOWS: boolean = true;
const DEFAULT_GREY_THRESHOLD: number = 135;

interface TMog2Config {
		history?: number;
		threshold?: number;
		detectShadows?: boolean;
}
function isTMog2Config(test: unknown): test is TMog2Config {
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'history')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'threshold')) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(test, 'detectShadows')) return false;
	
	return true;
}

interface TKnnConfig {
		history?: number;
		threshold?: number;
		detectShadows?: boolean;
}
function isTKnnConfig(test: unknown): test is TKnnConfig {
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'history')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'threshold')) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(test, 'detectShadows')) return false;
	
	return true;
}

export interface IConfig {
		minBlobPxSize: number;
		maxBlobPxSize: number;
		mog2?: TMog2Config;
		knn?: TKnnConfig;
		greyThreshold?: number;
}
export function isIConfig(test: unknown): test is IConfig {
	if (!CommonsType.hasPropertyNumber(test, 'minBlobPxSize')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'maxBlobPxSize')) return false;
	
	if (!CommonsType.hasPropertyTOrUndefined(test, 'mog2', isTMog2Config)) return false;
	if (!CommonsType.hasPropertyTOrUndefined(test, 'knn', isTKnnConfig)) return false;
	
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'greyThreshold')) return false;
	
	return true;
}

export class BlobDetector {
	private static fromConfigExpanded(
			minBlobPxSize: number,
			maxBlobPxSize: number,
			mog2: TMog2Config,
			knn: TKnnConfig,
			greyThreshold: number,
			viewer?: SplitViewer
	): BlobDetector {
		return new BlobDetector(
				minBlobPxSize,
				maxBlobPxSize,
				CommonsType.valueOrDefault<number>(mog2.history, DEFAULT_MOG2_HISTORY),
				CommonsType.valueOrDefault<number>(mog2.threshold, DEFAULT_MOG2_THRESHOLD),
				CommonsType.valueOrDefault<boolean>(mog2.detectShadows, DEFAULT_MOG2_DETECT_SHADOWS),
				CommonsType.valueOrDefault<number>(knn.history, DEFAULT_KNN_HISTORY),
				CommonsType.valueOrDefault<number>(knn.threshold, DEFAULT_KNN_THRESHOLD),
				CommonsType.valueOrDefault<boolean>(knn.detectShadows, DEFAULT_KNN_DETECT_SHADOWS),
				greyThreshold,
				viewer
		);
	}
	
	public static fromConfig(
			config: IConfig,
			viewer?: SplitViewer
	): BlobDetector {
		if (!isIConfig(config)) throw new Error('Invalid BlobDetector config');
		
		return BlobDetector.fromConfigExpanded(
				config.minBlobPxSize,
				config.maxBlobPxSize,
				CommonsType.valueOrDefault<TMog2Config>(
						config.mog2,
						{
								history: undefined,
								threshold: undefined,
								detectShadows: undefined,
						}
				),
				CommonsType.valueOrDefault<TKnnConfig>(
						config.knn,
						{
								history: undefined,
								threshold: undefined,
								detectShadows: undefined,
						}
				),
				CommonsType.valueOrDefault<number>(config.greyThreshold, DEFAULT_GREY_THRESHOLD),
				viewer
		);
	}
	
	private mixedBgSub: MixedBackgroundSubtractor;

	constructor(
			private readonly minBlobPxSize: number,
			private readonly maxBlobPxSize: number,
			readonly mog2History: number = DEFAULT_MOG2_HISTORY,
			readonly mog2Threshold: number = DEFAULT_MOG2_THRESHOLD,
			readonly mog2DetectShadows: boolean = DEFAULT_MOG2_DETECT_SHADOWS,
			readonly knnHistory: number = DEFAULT_KNN_HISTORY,
			readonly knnThreshold: number = DEFAULT_KNN_THRESHOLD,
			readonly knnDetectShadows: boolean = DEFAULT_KNN_DETECT_SHADOWS,
			private readonly greyThreshold: number = DEFAULT_GREY_THRESHOLD,
			private viewer?: SplitViewer
	) {
		this.mixedBgSub = new MixedBackgroundSubtractor(
				mog2History,
				mog2Threshold,
				mog2DetectShadows,
				knnHistory,
				knnThreshold,
				knnDetectShadows,
				3, 2,
				viewer
		);
	}
	
	private preview(window: string, image: cv.Mat): void {
		if (this.viewer) this.viewer.drawWindow(window, image, cv.COLOR_GRAY2BGR);
	}
	
	public detect(frame: cv.Mat): cv.Rect[] {
		const foreground: cv.Mat = this.mixedBgSub.apply(frame);

		const thresholded: cv.Mat = foreground.threshold(this.greyThreshold, 255, cv.THRESH_BINARY);
		this.preview('THRESHOLDED', thresholded.bitwiseNot());

		const {
				centroids,
				stats
		}: {
				centroids: cv.Mat,
				stats: cv.Mat
		} = thresholded.connectedComponentsWithStats();

		const detected: cv.Rect[] = [];
		
		// pretend label 0 is background
		for (let label = 1; label < centroids.rows; label++) {
			const size: number = stats.at(label, cv.CC_STAT_AREA);
			
			if (this.minBlobPxSize < size && size < this.maxBlobPxSize) {
				const rect: cv.Rect = new cv.Rect(
						stats.at(label, cv.CC_STAT_LEFT),
						stats.at(label, cv.CC_STAT_TOP),
						stats.at(label, cv.CC_STAT_WIDTH),
						stats.at(label, cv.CC_STAT_HEIGHT)
				);
				
				detected.push(rect);
			}
		}

		return detected;
	}
}
