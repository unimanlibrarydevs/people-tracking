import * as cv from 'opencv4nodejs';

export abstract class Source {
	public abstract nextFrame(): cv.Mat|undefined;
	public abstract getFrameRate(): number;
}
