import * as cv from 'opencv4nodejs';

import { SplitViewer } from './split-viewer.class';

export class MixedBackgroundSubtractor {
	private readonly bgSubtractorMog2: cv.BackgroundSubtractorMOG2;
	private readonly bgSubtractorKnn: cv.BackgroundSubtractorKNN;
	private erodeDilateSize: cv.Size;

	constructor(
			readonly mog2History: number = 500,
			readonly mog2Threshold: number = 16,
			readonly mog2DetectShadows: boolean = true,
			readonly knnHistory: number = 500,
			readonly knnThreshold: number = 400,
			readonly knnDetectShadows: boolean = true,
			readonly erodeDilateRadius: number = 3,
			private readonly erodeDilateIterations = 2,
			private readonly viewer?: SplitViewer
	) {
		this.bgSubtractorMog2 = new cv.BackgroundSubtractorMOG2(
				mog2History,
				mog2Threshold,
				mog2DetectShadows
		);
		
		this.bgSubtractorKnn = new cv.BackgroundSubtractorKNN(
				knnHistory,
				knnThreshold,
				knnDetectShadows
		);
		
		this.erodeDilateSize = new cv.Size(
				erodeDilateRadius,
				erodeDilateRadius
		);
	}

	private preview(window: string, image: cv.Mat): void {
		if (this.viewer) this.viewer.drawWindow(window, image, cv.COLOR_GRAY2BGR);
	}
	
	public apply(frame: cv.Mat): cv.Mat {
		const fgMask1: cv.Mat = this.bgSubtractorMog2.apply(frame);
		this.preview('MOG2', fgMask1.bitwiseNot());

		const fgMask2: cv.Mat = this.bgSubtractorKnn.apply(frame);
		this.preview('KNN', fgMask2.bitwiseNot());

		const blurred1: cv.Mat = fgMask1.blur(new cv.Size(8, 8));
		const blurred2: cv.Mat = fgMask2.blur(new cv.Size(8, 8));

		const mixed: cv.Mat = blurred1.addWeighted(0.5, blurred2, 0.5, 0);
		this.preview('BLUR-MIXED', mixed.bitwiseNot());

		const eroded: cv.Mat = mixed.erode(
				cv.getStructuringElement(cv.MORPH_ELLIPSE, this.erodeDilateSize),
				new cv.Point2(-1, -1),
				this.erodeDilateIterations
		);
		this.preview('ERODED', eroded.bitwiseNot());

		const dilated: cv.Mat = eroded.dilate(
				cv.getStructuringElement(cv.MORPH_ELLIPSE, this.erodeDilateSize),
				new cv.Point2(-1, -1),
				this.erodeDilateIterations
		);
		this.preview('DILATED', dilated.bitwiseNot());

		return dilated;
	}
}
