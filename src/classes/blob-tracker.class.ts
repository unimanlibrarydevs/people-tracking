import * as cv from 'opencv4nodejs';

import { CommonsType } from 'tscommons-core';

import { ITracked } from '../interfaces/itracked';

interface IInternalTracked extends ITracked {
		inTtl: number;
		outTtl: number;
		lastFrameIndex: number;
}

type TMatch = {
		tracked: IInternalTracked;
		deltaScore: number;
};

const DEFAULT_TRACK_TTL: number = 15;
const DEFAULT_MOVING_AVERAGE_PERIOD: number = 8;

function isRect(test: unknown): test is cv.Rect {
	if (!CommonsType.hasPropertyNumber(test, 'x')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'y')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'width')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'height')) return false;
	
	return true;
}

export interface IConfig {
		maxBlobDelta: cv.Rect;
		trackMemory: number;
		trackTtl?: number;
		movingAveragePeriod?: number;
}
export function isIConfig(test: unknown): test is IConfig {
	if (!CommonsType.hasPropertyT<cv.Rect>(test, 'maxBlobDelta', isRect)) return false;
	if (!CommonsType.hasPropertyNumber(test, 'trackMemory')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'trackTtl')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'movingAveragePeriod')) return false;
	
	return true;
}

export class BlobTracker {
	public static fromConfig(
			config: IConfig
	): BlobTracker {
		return new BlobTracker(
				config.maxBlobDelta,
				config.trackMemory,
				CommonsType.valueOrDefault<number>(config.trackTtl, DEFAULT_TRACK_TTL),
				CommonsType.valueOrDefault<number>(config.movingAveragePeriod, DEFAULT_MOVING_AVERAGE_PERIOD)
		);
	}
	
	private static centre(rect: cv.Rect): cv.Point2 {
		return new cv.Point2(
				rect.x + (rect.width / 2),
				rect.y + (rect.height / 2)
		);
	}
	
	private tracked: IInternalTracked[] = [];
	
	private currentId: number = 0;
	private frameIndex: number = 0;
	
	constructor(
			private maxBlobDelta: cv.Rect,
			private trackMemory: number,
			private trackTtl: number = DEFAULT_TRACK_TTL,
			private movingAveragePeriod: number = DEFAULT_MOVING_AVERAGE_PERIOD
	) {}
	
	private bestMatch(comparator: cv.Rect): IInternalTracked|undefined {
		const matches: TMatch[] = [];
		for (const potential of this.tracked) {
			const potentialCurrent: cv.Rect = potential.journey[potential.journey.length - 1];
			
			const frameIndexDelta: number = this.frameIndex - potential.lastFrameIndex;
			
			const comparatorCentre: cv.Point2 = BlobTracker.centre(comparator);
			const potentialCentre: cv.Point2 = BlobTracker.centre(potentialCurrent);
			
			let dx: number = Math.abs(comparatorCentre.x - potentialCentre.x);
			let dy: number = Math.abs(comparatorCentre.y - potentialCentre.y);
			
			const dw: number = Math.abs(comparator.width - potentialCurrent.width);
			const dh: number = Math.abs(comparator.height - potentialCurrent.height);
			
			// adjust to take into account longer time = longer distance travelled
			dx /= 1 + ((frameIndexDelta - 1) / this.trackMemory);
			dy /= 1 + ((frameIndexDelta - 1) / this.trackMemory);

			if (
					dx <= this.maxBlobDelta.x
					&& dy <= this.maxBlobDelta.y
					&& dw <= this.maxBlobDelta.width
					&& dh <= this.maxBlobDelta.height
			) {
				const deltaScore: number = dx + dy + dw + dh;
				matches.push({ tracked: potential, deltaScore: deltaScore });
			}
		}
		
		if (matches.length === 0) {
			// no matching existing tracked
			return undefined;
		}
		
		matches
				.sort((a: TMatch, b: TMatch): number => {
					if (a.deltaScore < b.deltaScore) return -1;
					if (a.deltaScore > b.deltaScore) return 1;
					return 0;
				});

		return matches[0].tracked;
	}
	
	public track(detections: cv.Rect[]): ITracked[] {
		this.frameIndex++;
		
		const matchedIds: number[] = [];
		
		for (const rect of detections) {
			const match: IInternalTracked|undefined = this.bestMatch(rect);
			
			if (match === undefined) {
				const incoming: IInternalTracked = {
						id: this.currentId++,
						journey: [ rect ],
						inTtl: this.trackTtl,
						outTtl: 0,
						lastFrameIndex: this.frameIndex
				}
				this.tracked.push(incoming);
				matchedIds.push(incoming.id);
				
				continue;
			}

			matchedIds.push(match.id);
			
			if (match.inTtl > 0) {
				// too recent, not exceeded incoming ttl
				match.inTtl--;
				continue;
			}

			// assume valid, so update
			match.journey.push(rect);
			match.outTtl = 0;
			match.lastFrameIndex = this.frameIndex;
		}
		
		const toRemove: number[] = [];
		for (const existing of this.tracked) {
			if (matchedIds.includes(existing.id)) {
				// previously matched, so skip
				continue;
			}
			
			// no match, consider removing
			if (existing.outTtl < this.trackTtl) {
				existing.outTtl++;
				continue;
			}
			
			toRemove.push(existing.id);
		}
		
		this.tracked = this.tracked
				.filter((t: IInternalTracked): boolean => !toRemove.includes(t.id));
		
		return this.tracked
				.map((t: IInternalTracked): ITracked => ({
					id: t.id,
					journey: t.journey
				}));
	}
	
	public journey(tracked: ITracked): cv.Point2[] {
		const steps: cv.Point2[] = tracked.journey
				.map((rect: cv.Rect): cv.Point2 => BlobTracker.centre(rect));
		
		if (steps.length < this.movingAveragePeriod) return [];
		const movingAverage: cv.Point2[] = steps.slice(0, this.movingAveragePeriod);
		const remaining: cv.Point2[] = steps.slice(this.movingAveragePeriod);
		
		const journey: cv.Point2[] = [];
		for (const step of remaining) {
			let avgX: number = 0;
			let avgY: number = 0;
			for (const p of movingAverage) {
				avgX += p.x;
				avgY += p.y;
			}
			
			avgX /= this.movingAveragePeriod;
			avgY /= this.movingAveragePeriod;
			
			const point: cv.Point2 = new cv.Point2(avgX, avgY);
			journey.push(point);
			
			movingAverage.shift();
			movingAverage.push(step);
		}
		
		return journey;
	}
}
