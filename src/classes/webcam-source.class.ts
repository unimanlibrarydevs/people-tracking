import * as cv from 'opencv4nodejs';

import { CommonsType } from 'tscommons-core';

import { Source } from './source.class';

const DEFAULT_DEVICE: number = 0;
const DEFAULT_CAPTURE_SIZE: cv.Size = new cv.Size(800, 600);

function isSize(test: unknown): test is cv.Size {
	if (!CommonsType.hasPropertyNumber(test, 'width')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'height')) return false;
	
	return true;
}

export interface IConfig {
		frameRate: number,
		device?: number,
		size?: cv.Size
}
export function isIConfig(test: unknown): test is IConfig {
	if (!CommonsType.hasPropertyNumber(test, 'frameRate')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'device')) return false;
	if (!CommonsType.hasPropertyTOrUndefined<cv.Size>(test, 'size', isSize)) return false;
	
	return true;
}

export class WebcamSource extends Source {
	public static fromConfig(
			config: IConfig
	): WebcamSource {
		return new WebcamSource(
				config.frameRate,
				CommonsType.valueOrDefault<number>(config.device, DEFAULT_DEVICE),
				CommonsType.valueOrDefault<cv.Size>(config.size, DEFAULT_CAPTURE_SIZE)
		);
	}
	
	private capture: cv.VideoCapture;
	
	constructor(
			private frameRate: number,
			device: number = DEFAULT_DEVICE,
			size: cv.Size = DEFAULT_CAPTURE_SIZE
	) {
		super();
		
		this.capture = new cv.VideoCapture(device);
		this.capture.set(3, size.width);
		this.capture.set(4, size.height);
	}
	
	public nextFrame(): cv.Mat|undefined {
		const frame: cv.Mat = this.capture.read();
		if (frame.empty) return undefined;
		
		return frame;
	}
	
	public getFrameRate(): number {
		return this.frameRate;
	}
}
