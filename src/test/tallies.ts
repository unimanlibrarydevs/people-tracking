import * as cv from 'opencv4nodejs';

import { TPropertyObject } from 'tscommons-core';

import { CommonsArgs } from 'nodecommons-cli';
import { CommonsConfigFile } from 'nodecommons-config';

import { Source } from '../classes/source.class';
import { VideoSource, isIConfig as isIVideoSourceConfig } from '../classes/video-source.class';
import { WebcamSource, isIConfig as isIWebcamSourceConfig } from '../classes/webcam-source.class';
import { SplitViewer } from '../classes/split-viewer.class';
import { BlobDetector } from '../classes/blob-detector.class';
import { BlobTracker } from '../classes/blob-tracker.class';
import { Graphics } from '../classes/graphics.class';

import { EDirection } from '../enums/edirection';

import { ITracked } from '../interfaces/itracked';
import { isIConfig } from '../interfaces/iconfig';

const args: CommonsArgs = new CommonsArgs();
const configFile: string = args.getString('config');

const config: TPropertyObject = CommonsConfigFile.readFileAsPropertyObject(`${__dirname}/../../config/${configFile}`);
if (!isIConfig(config)) throw new Error('Invalid configuration file');

let source: Source|undefined;

if (isIVideoSourceConfig(config.source)) {
	source = VideoSource.fromConfig(config.source);
} else if (isIWebcamSourceConfig(config.source)) {
	source = WebcamSource.fromConfig(config.source);
}

if (source === undefined) throw new Error('Invalid configuration file (no valid source)');

const frameRate: number = source.getFrameRate();

const splitViewer: SplitViewer = new SplitViewer(
		[
				[ 'SOURCE', 'MOG2', 'KNN' ],
				[ 'BLUR-MIXED', 'ERODED', 'DILATED' ],
				[ 'THRESHOLDED', 'JOURNEY', 'TALLIES' ]
		],
		600,
		320
);
const detector: BlobDetector = BlobDetector.fromConfig(config.detector, splitViewer);
const tracker: BlobTracker = BlobTracker.fromConfig(config.tracker);

const yThreshold: number = 250;
const thresholdSize: number = 5;
const yUpThreshold: number = yThreshold - (thresholdSize / 2);
const yDownThreshold: number = yThreshold + (thresholdSize / 2);

let tallyIn: number = 0;
let tallyOut: number = 0;
let pause: boolean = false;

type TState = {
		first: cv.Point2,
		last?: cv.Point2,
		state?: EDirection
}

const persons: Map<number, TState[]> = new Map<number, TState[]>();
//const trackedStates: Map<number, EDirection> = new Map<number, EDirection>();

const trigger: (source: Source) => Promise<void> = async (source: Source): Promise<void> => {
	const frame: cv.Mat|undefined = source.nextFrame();
	if (frame === undefined) { process.exit(0); throw new Error(''); }
	
	splitViewer.drawWindow('SOURCE', frame, undefined);

	const g1: Graphics = new Graphics(frame);
	
	const detections: cv.Rect[] = detector.detect(frame);
	g1.drawDetections(detections);
	
	const ts: ITracked[] = tracker.track(detections);
	const journeys: cv.Point2[][] = ts
			.map((t: ITracked): cv.Point2[] => tracker.journey(t));
	g1.drawJourneys(journeys);
	
	splitViewer.drawWindow('JOURNEY', g1.getFrame());
	
	const g2: Graphics = new Graphics(frame);
	
	g2.drawThreshold(yUpThreshold);
	g2.drawThreshold(yDownThreshold);
	
	for (const t of ts) {
		const steps: cv.Point2[] = tracker.journey(t).slice();

		const absoluteFirst: cv.Point2|undefined = steps.shift()!;
		if (!absoluteFirst) continue;
		const absoluteLast: cv.Point2|undefined = steps.pop()!;
		if (!absoluteLast) continue;
		
		if (!persons.has(t.id)) {
			persons.set(t.id, [ {
					first: absoluteFirst,
			} ]);
		}
		
		const states: TState[] = persons.get(t.id)!;
		const currentState: TState = states[states.length - 1];
		
		let i: number = 0;
		for (const state of states.slice(0, states.length - 1)) {
			if (state.last === undefined || state.state === undefined) {
				throw new Error('Either last or state is undefined. Should not be possible');
			}
			
			g2.drawTracked(state.first, state.last, state.state, i);
			i++;
		}

		const currentFirst: cv.Point2 = currentState.first;
		const currentLast: cv.Point2 = absoluteLast;
		
//		if (!trackedStates.has(t.id)) {
			if (currentFirst.y < yUpThreshold && currentLast.y > yDownThreshold) {
				// down
				currentState.last = currentLast;
				currentState.state = EDirection.DOWN;
				states.push({ first: currentLast });
				
				tallyOut++;
			}
			if (currentFirst.y > yDownThreshold && currentLast.y < yUpThreshold) {
				// up
				currentState.last = currentLast;
				currentState.state = EDirection.UP;
				states.push({ first: currentLast });

				tallyIn++;
			}
//		}
		
		g2.drawTracked(currentFirst, currentLast, undefined, i);
	}

	g2.drawTallies(tallyIn, tallyOut, tallyIn - tallyOut);
	
	splitViewer.drawWindow('TALLIES', g2.getFrame());
	
	const key: number|undefined = splitViewer.display(1);
	if (key !== undefined) {
		if (key === 113) { process.exit(0); throw new Error(''); }
		console.log('Key pressed. Pausing');
		pause = true;
	}
}

setInterval(
		(): void => {
			if (pause) {
				const key: number|undefined = splitViewer.display(100);
				if (key !== undefined) {
					if (key === 113) { process.exit(0); throw new Error(''); }
					console.log('Key pressed. Resuming');
					pause = false;
				}
				return;
			}
			
			if (source) trigger(source);
		},
		1000 / frameRate
);
