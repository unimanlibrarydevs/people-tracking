#!/bin/bash
rsync -av --delete ../../git/typescript/typescript-commons/src/lib/typescript-commons/ src/lib/typescript-commons

mkdir src/lib/node-commons 2>/dev/null
rsync -av --delete ../../git/node/node-commons/src/lib/node-commons/config/ src/lib/node-commons/config
rsync -av --delete ../../git/node/node-commons/src/lib/node-commons/cli/ src/lib/node-commons/cli
rsync -av --delete ../../git/node/node-commons/src/lib/node-commons/file/ src/lib/node-commons/file
