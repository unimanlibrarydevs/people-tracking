import { Demo1App } from '../apps/demo1.app';

import { Detector } from '../classes/detector.class';
import { Viewer } from '../classes/viewer.class';
import { Tracker } from '../classes/tracker.class';

import { crowd2Config } from '../config/crowd2.config';

const detector: Detector = new Detector(
		crowd2Config.minPxSize,
		crowd2Config.maxPxSize,
//		crowd2Config.maxDelta,
//		crowd2Config.ttl,
//		crowd2Config.movingAveragePeriod,
//		crowd2Config.yThreshold,
//		crowd2Config.thresholdWidth
);

const tracker: Tracker = new Tracker(
		crowd2Config.maxDeltas,
		crowd2Config.ttl,
		crowd2Config.movingAveragePeriod,
		crowd2Config.yThreshold,
		crowd2Config.thresholdWidth
);

const viewer: Viewer = new Viewer(
		crowd2Config.yThreshold,
		crowd2Config.thresholdWidth
);

const app: Demo1App = new Demo1App(
		detector,
		tracker,
		viewer,
//		crowd2Config.maxDelta,
//		crowd2Config.ttl,
//		crowd2Config.movingAveragePeriod,
//		crowd2Config.yThreshold,
//		crowd2Config.thresholdWidth
)
//app.run('./data/crowd2.mp4', crowd2Config.frameRate);
app.run(0, crowd2Config.frameRate);
