import * as cv from 'opencv4nodejs';

const cap: cv.VideoCapture = new cv.VideoCapture(0);
cap.set(3, 800);
cap.set(4, 600);

const bgSubtractor: cv.BackgroundSubtractorMOG2 = new cv.BackgroundSubtractorMOG2();

const interval: any = setInterval(
		(): void => {
			let frame: cv.Mat = cap.read();
			
			if (frame.empty) {
				clearInterval(interval);
				return;
			}

			const foreGroundMask: cv.Mat = bgSubtractor.apply(frame);

			const blurred: cv.Mat = foreGroundMask.blur(new cv.Size(8, 8));
			
			const iterations: number = 2;
			const dilated: cv.Mat = blurred.dilate(
					cv.getStructuringElement(cv.MORPH_ELLIPSE, new cv.Size(3, 3)),
					new cv.Point2(-1, -1),
					iterations
			);
			let t: number = 1;
			if (t === 2) {
				console.log(dilated);
			}

			const blurred2: cv.Mat = dilated.blur(new cv.Size(8, 8));
			const thresholded: cv.Mat = blurred2.threshold(200, 255, cv.THRESH_BINARY);
			
			const temp: cv.Mat = thresholded.bitwiseNot().cvtColor(cv.COLOR_GRAY2BGR);
			const result: cv.Mat = frame.add(temp);
			cv.imshow('frame', result);
			
			const key: number = cv.waitKey(1);
			if (key !== -1 && key !== 255) {
				console.log('Key pressed, exiting.');
				clearInterval(interval);
				return;
			}
		},
		10
);
