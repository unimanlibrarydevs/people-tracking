import * as cv from 'opencv4nodejs';

import { Detector } from '../classes/detector.class';
import { Viewer } from '../classes/viewer.class';
import { Tracker } from '../classes/tracker.class';

import { ITracked } from '../interfaces/itracked';

import { TDualPoint } from '../types/tdual-point';

import { EDirection } from '../enums/edirection';

export class Demo1App {
	private interval: any|undefined;
	
//	private tracker: Tracker;

	constructor(
			private detector: Detector,
			private tracker: Tracker,
			private viewer: Viewer,
//			maxDelta: number,
//			ttl: number,
//			movingAveragePeriod: number,
//			yThreshold: number,
//			thresholdWidth: number
	) {
//		this.tracker = new Tracker(
//				maxDelta,
//				ttl,
//				movingAveragePeriod,
//				yThreshold,
//				thresholdWidth
//		);
	}
	
	public run(
			videoFile: number,
			frameRate: number
	): void {
		let inTally: number = 0;
		let outTally: number = 0;
		
		const cap: cv.VideoCapture = new cv.VideoCapture(videoFile);
		this.interval = setInterval(
				(): void => {
					let frame: cv.Mat = cap.read();
					
					if (frame.empty) {
						console.log(`------------------\nFinal results:`);
						console.log(`IN: ${inTally}; OUT: ${outTally}; BALANCE: ${inTally - outTally}`);
						
						clearInterval(this.interval);
						return;
					}

					const centroids: TDualPoint[] = this.detector.scanFrame(frame);
					
					//frame = this.detector.temp || frame;
					
					this.viewer.drawCentroids(frame, centroids);

					const trackeds: ITracked[] = this.tracker.track(centroids);
					
					for (const tracked of trackeds) {
						const journey: cv.Point2[] = this.tracker.journey(tracked);
						this.viewer.drawJourney(frame, journey);
						
						const check: EDirection|undefined = this.tracker.checkThresholds(tracked);
						if (check !== undefined) {
							console.log(`${check} for ${tracked.id}`);
						
							switch (check) {
								case EDirection.UP:
									inTally++;
									break;
								case EDirection.DOWN:
									outTally++;
									break;
							}
							
							console.log(`IN: ${inTally}; OUT: ${outTally}; BALANCE: ${inTally - outTally}`);
						}
					}

					this.viewer.drawThresholds(frame);
					
					cv.imshow('frame', frame);
					
					const key: number = cv.waitKey(1);
					if (key !== -1 && key !== 255) {
						console.log('Key pressed, exiting.');
						clearInterval(this.interval);
						return;
					}
				},
				1000 / frameRate
		);
	}
}
